use ray::V3;
use hitable::HitRecord;
use ray::Ray;


//NOTE (goost) Tagged for removal
pub trait Light: Sync + Send {
    fn get_pos(&self) -> V3;
    fn get_color(&self,r_in: &Ray, rec: &HitRecord) -> V3;
    fn get_radius(&self) -> f64;
}