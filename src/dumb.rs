fn dumb () {
  thread::spawn(move || {
        for y in (0..ny - 1).rev() {
            for x in 0..nx {
                let r = x as f32 / nx as f32;
                let g = y as f32 / ny as f32;
                let b = 0.2f32;
                let r = (255.99 * r) as u8;
                let g = (255.99 * g) as u8;
                let b = (255.99 * b) as u8;
                let pix = Rgba([r, g, b, 255]);
                img.put_pixel(x as u32, y as u32, pix);
               // traceimage.update(&mut encoder, &img);
            }
        }
        write_png_image("trace.png", &img);
    });
}

fn write_image_vector(filepath: &str, data: &[Vec<Color>]) -> Result<(), std::io::Error> {
    let mut f = File::create(filepath).expect("Unable to create file");
    writeln!(&mut f, "P3")?;
    writeln!(&mut f, "200  100")?;
    writeln!(&mut f, "255")?;
    for i in data {
        for j in i {
            writeln!(&mut f, "{} {} {}", j.data[0], j.data[1], j.data[2])?;
        }
    }
    Ok(())
}