use aabb::AABB;
use hitable::HitRecord;
use hitable::Hitable;
use material::Material;
use ray::Ray;
use ray::V3;
use std::sync::Arc;
use light::Light;
use std::f64;
use onb;
use pdf;

pub struct Sphere {
  center: V3,
  radius: f64,
  material: Arc<Material>,
}

impl Sphere {
  #[allow(dead_code)]
  pub fn new(center: V3, radius: f64, material: Arc<Material>) -> Sphere {
    Sphere {
      center,
      radius,
      material,
    }
  }

  pub fn boxed(center: V3, radius: f64, material: Arc<Material>) -> Box<Sphere> {
    Box::new(Sphere {
      center,
      radius,
      material,
    })
  }

  fn normal(&self, pos: &V3) -> V3 {
    (pos - self.center) / self.radius
  }

  fn get_shpere_uv(p: &V3) -> (f64, f64) {
    use std::f64::consts::PI;
    let phi = p.z.atan2(p.x);
    let theta = p.y.asin();
    (1.0 - (phi + PI) / (2.0 * PI), (theta + PI / 2.0) / PI)
  }
}

impl Light for Sphere {
  fn get_pos(&self) -> V3 {
    self.center
  }

  fn get_color(&self,r_in: &Ray, rec: &HitRecord,) -> V3 {
    (self.material.emitted)(r_in, rec,0.0,0.0,&V3::zeros())
  }

  fn get_radius(&self) -> f64 {
    self.radius
  }
}

impl Hitable for Sphere {
  fn pdf_value(&self, o: &V3, v: &V3) -> f64 {
      match self.hit(&Ray::new(*o, *v), 0.001, f64::MAX) {
        Some(ref rec) => {
          //let area = 4.0 * f64::consts::PI * self.radius * self.radius;
          //let distance_squared = rec.t * rec.t * v.norm_squared();
          //let cosine = v.dot(&rec.normal) / v.norm();
          //let cosine = cosine.abs();
          //distance_squared / (cosine * area)
          //NOTE(goost) Above alternative?
          let t = 1.0 - self.radius*self.radius;
          let t2 = self.center - o;
          //NOTE (goost) Which one is correct?
          let cos_theta_max = (t2 * (1.0/t)).norm_squared().sqrt();
          //let cos_theta_max = (t2 / t).norm_squared().sqrt();
          let solid_angle = 2.0 * f64::consts::PI * (1.0 - cos_theta_max);
          1.0 / solid_angle

        },
        None => 0.0
      }
  }
  fn random(&self, o: &V3) -> V3 {
      let direction = self.center - o;
      let distance_squared = direction.norm_squared();
      let uvw = onb::ONB::build_from_w(&direction);
      uvw.local_vec(&pdf::random_to_sphere(self.radius, distance_squared))
  }


  fn count_primitives(&self) -> u32 {
    1
  }

  fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
    let oc = r.origin() - self.center;
    let a = r.direction().dot(&r.direction());
    let b = oc.dot(&r.direction());
    let c = oc.dot(&oc) - self.radius * self.radius;
    let discriminant = b * b - a * c;
    if discriminant > 0f64 {
      let temp = (-b - discriminant.sqrt()) / a;
      if temp < t_max && temp > t_min {
        let pos = r.point_at(temp);
        let (u, v) = Sphere::get_shpere_uv(&pos);
        return Some(HitRecord::new(
          temp,
          pos,
          self.normal(&pos),
          &self.material,
          u,
          v,
        ));
      }
      let temp = (-b + discriminant.sqrt()) / a;
      if temp < t_max && temp > t_min {
        let pos = r.point_at(temp);
        let (u, v) = Sphere::get_shpere_uv(&pos);
        return Some(HitRecord::new(
          temp,
          pos,
          self.normal(&pos),
          &self.material,
          u,
          v,
        ));
      }
    }
    None
  }

  fn bounding_box(&self, _t0: f64, _t1: f64) -> Option<AABB> {
    Some(AABB::new(
      self.center - V3::new(self.radius, self.radius, self.radius),
      self.center + V3::new(self.radius, self.radius, self.radius),
    ))
  }
}
