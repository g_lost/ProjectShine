use aabb::AABB;
use hitable::HitRecord;
use hitable::Hitable;
use material::Material;
use ray::Ray;
use ray::V3;
use std::sync::Arc;
//NOTE (goost) Source https://github.com/andystanton/raytracer-rs/blob/master/src/triangle.rs

pub struct Triangle {
    vertices: [V3; 3],
    normals: [V3; 3],
    material: Arc<Material>,
}

#[allow(dead_code)]
impl Triangle {
    pub fn new(vertices: [V3; 3], normals: [V3; 3], material: Arc<Material>) -> Self {
        Self {
            vertices,
            normals,
            material,
        }
    }

    pub fn boxed(vertices: [V3; 3], normals: [V3; 3], material: Arc<Material>) -> Box<Self> {
        Box::new(Self {
            vertices,
            normals,
            material,
        })
    }
}

impl Hitable for Triangle {
    fn count_primitives(&self) -> u32 {
        1
    }

    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let e1 = self.vertices[1] - self.vertices[0];
        let e2 = self.vertices[2] - self.vertices[0];

        let pvec = r.direction().cross(&e2);
        let det = e1.dot(&pvec);
        if det > -0.00001 && det < 0.00001 {
            return None;
        }

        let inv_det = 1.0 / det;
        let tvec = r.origin() - self.vertices[0];
        let u = tvec.dot(&pvec) * inv_det;
        if u < 0.0 || u > 1.0 {
            return None;
        }

        let qvec = tvec.cross(&e1);
        let v = r.direction().dot(&qvec) * inv_det;

        if v < 0.0 || u + v > 1.0 {
            return None;
        }

        let t = e2.dot(&qvec) * inv_det;

        let pos = r.point_at(t);

        if t > 0.00001 && t < t_max && t > t_min {
            let normal =
                u * self.normals[1] + v * self.normals[2] + (1.0 - u - v) * self.normals[0];
            Some(HitRecord::new(t, pos, normal, &self.material, 0.0, 0.0))
        } else {
            None
        }
    }

    fn bounding_box(&self, _: f64, _: f64) -> Option<AABB> {
        let mut min = V3::new(
            self.vertices[0].x.min(self.vertices[1].x),
            self.vertices[0].y.min(self.vertices[1].y),
            self.vertices[0].z.min(self.vertices[1].z),
        );
        min = V3::new(
            min.x.min(self.vertices[2].x),
            min.y.min(self.vertices[2].y),
            min.z.min(self.vertices[2].z),
        );

        let mut max = V3::new(
            self.vertices[0].x.max(self.vertices[1].x),
            self.vertices[0].y.max(self.vertices[1].y),
            self.vertices[0].z.max(self.vertices[1].z),
        );
        max = V3::new(
            max.x.max(self.vertices[2].x),
            max.y.max(self.vertices[2].y),
            max.z.max(self.vertices[2].z),
        );

        Some(AABB::new(min, max))
    }
}
