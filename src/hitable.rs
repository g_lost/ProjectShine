use ray::V3;
use ray::Ray;
use material::Material;
use ordered_float::OrderedFloat;
use aabb::AABB;
use std::f64;
use rand;

pub struct HitRecord<'a> {
    pub u: f64,
    pub v: f64,
    pub t: f64,
    pub pos: V3,
    pub normal: V3,
    pub material: &'a Material
}

impl<'a> HitRecord<'a> {
  pub fn new (t: f64, pos: V3, normal: V3, material: &Material, u: f64, v:f64) -> HitRecord {
    //let material = Box::new(mat);
    HitRecord{
        u,
        v,
        t,
        pos,
        normal,
        material
    }
  }
}


pub trait Hitable: Send + Sync {
  fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord>;
  fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB>;
 // fn get_primitives(self) -> Vec<Box<Hitable>>;
  fn count_primitives(&self) -> u32;
  fn pdf_value(&self, o: &V3, v: &V3) -> f64 {
      0.0
  }
  fn random(&self, o: &V3) -> V3 {
      V3::new(1.0,0.0,0.0)
  }
}

impl Hitable for [Box<Hitable>] {
    fn pdf_value(&self, o: &V3, v: &V3) -> f64 {
      let weight = 1.0/ self.len() as f64;
      self.iter().map(|h| h.pdf_value(o, v)*weight).sum()
    }

    fn random(&self, o: &V3) -> V3 {
        let index = rand::random::<f64>() * (self.len() as f64);
        self[index as usize].random(o)
    }

    fn count_primitives(&self) -> u32 {
        self.iter().map(|h| h.count_primitives()).sum()
    }

    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        self.iter()
            .filter_map(|h| h.hit(r, t_min, t_max))
            //.filter(|rec| rec.is_some())
            //.map(|s| s.unwrap())
            .min_by_key(|n| OrderedFloat(n.t))
    }

     fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        if self.is_empty() {
            None
        } else {
             self[1..].iter().fold(self[0].bounding_box(t0, t1), |acc, next| {
                    acc.and_then(|b| next.bounding_box(t0, t1).map(|n| b.surrounding_box(&n)))
                })
        }
    }
}

impl Hitable for Vec<Box<Hitable>> {
    fn pdf_value(&self, o: &V3, v: &V3) -> f64 {
      let weight = 1.0/ self.len() as f64;
      self.iter().map(|h| h.pdf_value(o, v)*weight).sum()
    }

    fn random(&self, o: &V3) -> V3 {
        let index = rand::random::<f64>() * (self.len() as f64);
        self[index as usize].random(o)
    }

    fn count_primitives(&self) -> u32 {
        self.iter().map(|h| h.count_primitives()).sum()
    }

    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        self.iter()
            .filter_map(|h| h.hit(r, t_min, t_max))
            //.filter(|rec| rec.is_some())
            //.map(|s| s.unwrap())
            .min_by_key(|n| OrderedFloat(n.t))
    }

     fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        if self.is_empty() {
            None
        } else {
             self[1..].iter().fold(self[0].bounding_box(t0, t1), |acc, next| {
                    acc.and_then(|b| next.bounding_box(t0, t1).map(|n| b.surrounding_box(&n)))
                })
        }
    }
}
