use camera::Camera;
use hitable::HitRecord;
use hitable::Hitable;
use light::Light;
use material::Material;
use mesh::Mesh;
use rand;
use ray::Quaternion;
use ray::V3;
use scene::Scene;
use sphere::Sphere;
use std::f64;
use std::sync::Arc;
use teapot::Teapot;
use texture::CheckerTexture;
use texture::ConstantTexture;
use tree::Tree;
use na::Vector3;

#[allow(unused_variables, dead_code)]
pub fn default(aspect: f64) -> (Scene, Camera) {
    println!("Using default scene.");

    let origin = V3::new(2.0, 2.0, 10.0);
    let loot_at = V3::new(0.0, 0.0, 0.0);
    let dist_to_focus = 10.0;
    let aperture = 0.1;
    let camera = Camera::new(
        origin,
        loot_at,
        V3::new(0.0, 1.0, 0.0),
        20.0,
        aspect,
        aperture,
        dist_to_focus,
    );
    //println!("{:#?}", camera);

    let blue = Material::lambertian(ConstantTexture::boxed(V3::new(0.0, 0.0, 1.0)));
    let red = Material::lambertian(ConstantTexture::boxed(V3::new(1.0, 0.0, 0.0)));
    let reddish = Material::lambertian(ConstantTexture::boxed(V3::new(0.8, 0.3, 0.3)));
    let blueish = Material::lambertian(ConstantTexture::boxed(V3::new(0.1, 0.2, 0.5)));
    let ockar = Material::lambertian(ConstantTexture::boxed(V3::new(0.8, 0.8, 0.0)));
    let pink_metal_fuzzy = Material::metal(ConstantTexture::boxed(V3::new(0.8, 0.6, 0.8)), 1.0);
    let color_metal_shiny = Material::metal(ConstantTexture::boxed(V3::new(0.2, 0.6, 0.6)), 0.0);
    let lightgrey_metal_shiny =
        Material::metal(ConstantTexture::boxed(V3::new(0.8, 0.8, 0.8)), 0.2);
    let glass = Material::dielectric(1.5);
    let checker = Material::lambertian(CheckerTexture::boxed(
        ConstantTexture::boxed(V3::new(0.2, 0.3, 0.1)),
        ConstantTexture::boxed(V3::new(0.9, 0.9, 0.9)),
    ));
    let light = Material::diffuse_light(ConstantTexture::boxed(V3::new(1.0, 0.5, 1.0)), 4.0);
    let lightg = Material::diffuse_light(ConstantTexture::boxed(V3::new(0.5, 1.0, 1.0)), 4.0);
    let r = (f64::consts::PI / 4.0).cos();
    let light1 = Sphere::new(V3::new(-2.0, 3.0, -1.0), 0.75, Arc::clone(&light));
    let light2 = Sphere::new(V3::new(2.0, 3.0, -1.0), 0.75, Arc::clone(&lightg));
    //TODO (goost) Hack to silence borrow checker -> Lights/Spheres are not cloneable =( y?
    let lightc = Material::diffuse_light(ConstantTexture::boxed(V3::new(1.0, 0.5, 1.0)), 1.0 / 4.0);
    let lightcg =
        Material::diffuse_light(ConstantTexture::boxed(V3::new(0.5, 1.0, 1.0)), 1.0 / 4.0);
    let light1c = Sphere::new(V3::new(-2.0, 3.0, -1.0), 0.75, Arc::clone(&lightc));
    let light2c = Sphere::new(V3::new(2.0, 3.0, -1.0), 0.75, Arc::clone(&lightcg));
    let v: Vec<Box<Hitable>> = vec![
        //Box::new(Sphere::new(V3::new(-r, 0.0, -1.0), r, Arc::clone(&blue))),
        //Box::new(Sphere::new(V3::new(r, 0.0, -1.0), r, Arc::clone(&red)))
        Sphere::boxed(V3::new(0.0, 0.5, -1.0), 1.0, Arc::clone(&reddish)),
        Box::new(light1),
        Box::new(light2),
        Sphere::boxed(V3::new(0.0, -100.5, -1.0), 100.0, Arc::clone(&checker)),
        Sphere::boxed(V3::new(2.0, 0.5, -1.0), 1.0, Arc::clone(&color_metal_shiny)),
        Sphere::boxed(V3::new(-2.0, 0.5, -1.0), 1.0, Arc::clone(&glass)),
        Sphere::boxed(V3::new(-2.0, 0.5, -1.0), -0.95, Arc::clone(&glass)),
    ];
    let density: Vec<Box<Hitable>> = vec![Box::new(light1c), Box::new(light2c)];
    (Scene::new(Box::new(Tree::create(v, 2)), density), camera)
}

pub fn random(aspect: f64) -> (Scene, Camera) {
    println!("Using random scene.");
    let origin = V3::new(13.0, 2.0, 3.0);
    let loot_at = V3::new(0.0, 0.0, 0.0);
    let dist_to_focus = 10.0;
    let aperture = 0.1;
    let camera = Camera::new(
        origin,
        loot_at,
        V3::new(0.0, 1.0, 0.0),
        20.0,
        aspect,
        aperture,
        dist_to_focus,
    );
    //println!("{:#?}", camera);

    let n = 500;
    let mut v: Vec<Box<Hitable>> = Vec::with_capacity(n + 1);
    let checker = Material::lambertian(CheckerTexture::boxed(
        ConstantTexture::boxed(V3::new(0.2, 0.3, 0.1)),
        ConstantTexture::boxed(V3::new(0.9, 0.9, 0.9)),
    ));
    v.push(Sphere::boxed(V3::new(0.0, -1000.0, 0.0), 1000.0, checker));
    let glass = Material::dielectric(1.5);
    let bias = V3::new(4.0, 0.2, 0.0);
    for a in -11..11 {
        for b in -11..11 {
            let choose_mat = rand::random::<f64>();
            let center = V3::new(
                f64::from(a) + 0.9 * rand::random::<f64>(),
                0.2,
                f64::from(b) + 0.9 * rand::random::<f64>(),
            );
            if (center - bias).norm() > 0.9 {
                if choose_mat < 0.8 {
                    v.push(Sphere::boxed(
                        center,
                        0.2,
                        Material::lambertian(ConstantTexture::boxed(V3::new(
                            rand::random::<f64>() * rand::random::<f64>(),
                            rand::random::<f64>() * rand::random::<f64>(),
                            rand::random::<f64>() * rand::random::<f64>(),
                        ))),
                    ));
                } else if choose_mat < 0.95 {
                    v.push(Sphere::boxed(
                        center,
                        0.2,
                        Material::metal(
                            ConstantTexture::boxed(V3::new(
                                0.5 * (1.0 - rand::random::<f64>()),
                                0.5 * (1.0 - rand::random::<f64>()),
                                0.5 * (1.0 - rand::random::<f64>()),
                            )),
                            0.5 * rand::random::<f64>(),
                        ),
                    ));
                } else {
                    v.push(Sphere::boxed(center, 0.2, Arc::clone(&glass)));
                }
            }
        }
    }

    let light = Material::diffuse_light(ConstantTexture::boxed(V3::new(1.0, 1.0, 1.0)), 6.0);
    let light1 = Sphere::new(V3::new(0.0, 5.0, 0.0), 1.0, Arc::clone(&light));
    let light2 = Sphere::new(V3::new(-4.0, 5.0, -2.0), 1.0, Arc::clone(&light));
    let light3 = Sphere::new(V3::new(4.0, 5.0, 2.0), 1.0, Arc::clone(&light));
    //TODO (goost) Hack to silence borrow checker -> Lights/Spheres are not cloneable =( y?
    let lightc = Material::diffuse_light(ConstantTexture::boxed(V3::new(1.0, 1.0, 1.0)), 1.0 / 6.0);
    let light1c = Sphere::new(V3::new(0.0, 5.0, 0.0), 1.0, Arc::clone(&lightc));
    let light2c = Sphere::new(V3::new(-4.0, 5.0, -2.0), 1.0, Arc::clone(&lightc));
    let light3c = Sphere::new(V3::new(4.0, 5.0, 2.0), 1.0, Arc::clone(&lightc));
    v.push(Box::new(light1));
    v.push(Box::new(light2));
    v.push(Box::new(light3));
    v.push(Sphere::boxed(
        V3::new(0.0, 1.0, 0.0),
        1.0,
        Arc::clone(&glass),
    ));
    v.push(Sphere::boxed(
        V3::new(-4.0, 1.0, 0.0),
        1.0,
        Material::lambertian(ConstantTexture::boxed(V3::new(0.4, 0.2, 0.1))),
    ));
    v.push(Sphere::boxed(
        V3::new(4.0, 1.0, 0.0),
        1.0,
        Material::metal(ConstantTexture::boxed(V3::new(0.7, 0.6, 0.5)), 0.0),
    ));
    let density: Vec<Box<Hitable>> = vec![Box::new(light1c), Box::new(light2c), Box::new(light3c)];
    (Scene::new(Box::new(Tree::create(v, 7)), density), camera)
}

pub fn dungeon(aspect: f64) -> (Scene, Camera) {
    println!("Using dungeon scene.");
    let mut v: Vec<Box<Hitable>> = Vec::with_capacity(13);
    let behind_rocks_m =
        Material::diffuse_light(ConstantTexture::boxed(V3::new(1.0, 1.0, 1.0)), 6.0);
    let left_m = Material::diffuse_light(ConstantTexture::boxed(V3::new(1.0, 0.6, 1.0)), 2.0);
    let right_m = Material::diffuse_light(ConstantTexture::boxed(V3::new(0.6, 1.0, 0.6)), 2.0);
    let front_m = Material::diffuse_light(ConstantTexture::boxed(V3::new(1.0, 0.6, 0.6)), 2.0);
    let above_m = Material::diffuse_light(ConstantTexture::boxed(V3::new(1.0, 1.0, 1.0)), 2.0);

    let behind_rocks_l = Sphere::boxed(V3::new(7.2, 2.1, 0.4), 1.0, behind_rocks_m.clone());
    let left_l = Sphere::boxed(V3::new(-0.8, 6.09, 5.5), 2.0, left_m.clone());
    let right_l = Sphere::boxed(V3::new(-0.8, 6.09, -5.5), 2.0, right_m.clone());
    let front_l = Sphere::boxed(V3::new(-9.27, 5.38, -2.1), 2.0, front_m.clone());
    let above_l = Sphere::boxed(V3::new(2.7, 8.6, -0.25), 2.0, above_m.clone());
    //NOTE(goost) Borrower silencer TODO implementations without copy
    let behind_rocks_lc = Sphere::boxed(V3::new(7.2, 2.1, 0.4), 1.0, behind_rocks_m);
    let left_lc = Sphere::boxed(V3::new(-0.8, 6.09, 5.5), 2.0, left_m);
    let right_lc = Sphere::boxed(V3::new(-0.8, 6.09, -5.5), 2.0, right_m);
    let front_lc = Sphere::boxed(V3::new(-9.27, 5.38, -2.1), 2.0, front_m);
    let above_lc = Sphere::boxed(V3::new(2.7, 8.6, -0.25), 2.0, above_m);
    let density: Vec<Box<Hitable>> = vec![left_lc, right_lc, front_lc];

    //v.push(behind_rocks_l);
    v.push(left_l);
    v.push(right_l);
    v.push(front_l);
    //v.push(above_l);
    let rock_color = Vector3::<u8>::new(90,77,65);
    let rock = Material::lambertian(Box::new(ConstantTexture::from_rgb(&rock_color)));
    v.push(Box::new(Mesh::new(
        "assets/dungeon/rocks.obj",
        V3::zeros(),
        1.0,
        Quaternion::identity(),
        &rock,
    )));
    v.push(Box::new(Mesh::new(
        "assets/dungeon/ground.obj",
        V3::zeros(),
        1.0,
        Quaternion::identity(),
        &rock,
    )));
    let bones = Vector3::<u8>::new(89,85,79);
    v.push(Box::new(Mesh::new(
        "assets/dungeon/skeleton.obj",
        V3::zeros(),
        1.0,
        Quaternion::identity(),
        &Material::metal(Box::new(ConstantTexture::from_rgb(&bones)), 0.8),
    )));
    let dark_wood = Vector3::<u8>::new(68,48,34);
    v.push(Box::new(Mesh::new(
        "assets/dungeon/chest.obj",
        V3::zeros(),
        1.0,
        Quaternion::identity(),
        &Material::lambertian(Box::new(ConstantTexture::from_rgb(&dark_wood))),
    )));
    let gold = Vector3::<u8>::new(255,215,0);
    v.push(Box::new(Mesh::new(
        "assets/dungeon/gold.obj",
        V3::zeros(),
        1.0,
        Quaternion::identity(),
        &Material::metal(Box::new(ConstantTexture::from_rgb(&gold)), 0.3),
    )));
    let barrel = Vector3::<u8>::new(184,65,14);
    v.push(Box::new(Mesh::new(
        "assets/dungeon/shinyBarrels.obj",
        V3::zeros(),
        1.0,
        Quaternion::identity(),
        &Material::metal(Box::new(ConstantTexture::from_rgb(&barrel)), 0.001),
    )));
    let shiba = V3::new(0.849,0.329,0.051);
    v.push(Box::new(Mesh::new(
        "assets/dungeon/shiba.obj",
        V3::zeros(),
        1.0,
        Quaternion::identity(),
        &Material::lambertian(ConstantTexture::boxed(shiba)),
    )));
    

    let origin = V3::new(-9.09, 2.57, -0.03);
    //let origin = V3::new(-7.0, 7.0, 0.5);
    let at = V3::new(0.0, 0.0, 0.0);
    let dist_to_focus = (origin - at).norm();
    let aperture = 0.001;
    let camera = Camera::new(
        origin,
        at,
        V3::new(0.0, 1.0, 0.0),
        35.0,
        aspect,
        aperture,
        dist_to_focus,
    );
    (Scene::new(Box::new(Tree::create(v, 3)), density), camera)
}

pub fn simple_import(aspect: f64) -> (Scene, Camera) {
    println!("Using simpleImport scene.");
    let ground_level = 0.0;
    let scale = 1.0;
    let mut v: Vec<Box<Hitable>> = Vec::with_capacity(5 + 1);
    let checker = Material::lambertian(CheckerTexture::boxed(
        ConstantTexture::boxed(V3::new(0.2, 0.3, 0.1)),
        ConstantTexture::boxed(V3::new(0.9, 0.9, 0.9)),
    ));
    let light = Material::diffuse_light(ConstantTexture::boxed(V3::new(1.0, 1.0, 1.0)), 15.0);
    let light1 = Sphere::new(V3::new(-2.7, 5.0, 6.0), 2.0, Arc::clone(&light));
    let light2 = Sphere::new(V3::new(1.5, 5.0, -1.5), 2.0, Arc::clone(&light));
    let light1c = Sphere::new(V3::new(-2.7, 3.0, 4.0), 0.5, Arc::clone(&light));
    let light2c = Sphere::new(V3::new(1.5, 5.0, -1.5), 2.0, Arc::clone(&light));
    v.push(Box::new(light1));
    v.push(Box::new(light2));
    v.push(Box::new(Mesh::new(
        "assets/cone.obj",
        //V3::new(0.0, ground_level + scale /2.0, 4.0),
        V3::new(0.0, 0.0, 0.0),
        scale,
        Quaternion::identity(),
        &Material::lambertian(ConstantTexture::boxed(V3::new(0.8, 0.3, 0.1))),
    )));
    v.push(Box::new(Mesh::new(
        "assets/plane.obj",
        V3::new(0.0, 0.0, 0.0),
        scale,
        Quaternion::identity(),
        &Material::lambertian(ConstantTexture::boxed(V3::new(0.1, 0.2, 0.5))),
    )));
    v.push(Box::new(Mesh::new(
        "assets/icosphere.obj",
        //V3::new(0.0, ground_level + scale /2.0, 0.0),
        V3::zeros(),
        scale,
        Quaternion::identity(),
        &Material::metal(ConstantTexture::boxed(V3::new(0.7, 0.6, 0.5)), 0.01),
    )));

    let origin = V3::new(-7.0, 7.0, 0.5);
    let at = V3::new(0.0, 0.0, 0.0);
    let dist_to_focus = (origin - at).norm();
    let aperture = 0.001;
    let camera = Camera::new(
        origin,
        at,
        V3::new(0.0, 1.0, 0.0),
        30.0,
        aspect,
        aperture,
        dist_to_focus,
    );
    let density: Vec<Box<Hitable>> = vec![Box::new(light1c), Box::new(light2c)];
    (Scene::new(Box::new(Tree::create(v, 3)), density), camera)
}

pub fn teapot(aspect: f64) -> (Scene, Camera) {
    //NOte (goost) Source: https://github.com/andystanton/raytracer-rs/blob/master/src/scene.rs
    println!("Using teapot scene.");
    let ground_level = 0.0;
    let scale = 2.5;
    let mut v: Vec<Box<Hitable>> = Vec::with_capacity(5 + 1);
    let checker = Material::lambertian(CheckerTexture::boxed(
        ConstantTexture::boxed(V3::new(0.2, 0.3, 0.1)),
        ConstantTexture::boxed(V3::new(0.9, 0.9, 0.9)),
    ));
    let light = Material::diffuse_light(ConstantTexture::boxed(V3::new(1.0, 1.0, 1.0)), 12.0);
    let light1 = Sphere::new(V3::new(0.0, 15.0, 0.0), 2.0, Arc::clone(&light));
    let light2 = Sphere::new(V3::new(-4.0, 15.0, -2.0), 2.0, Arc::clone(&light));
    let light3 = Sphere::new(V3::new(4.0, 15.0, 2.0), 2.0, Arc::clone(&light));
    let lightc =
        Material::diffuse_light(ConstantTexture::boxed(V3::new(1.0, 1.0, 1.0)), 1.0 / 12.0);
    //TODO (goost) Hack to silence borrow checker -> Lights/Spheres are not cloneable =( y?
    let light1c = Sphere::new(V3::new(0.0, 15.0, 0.0), 2.0, Arc::clone(&lightc));
    let light2c = Sphere::new(V3::new(-4.0, 15.0, -2.0), 2.0, Arc::clone(&lightc));
    let light3c = Sphere::new(V3::new(4.0, 15.0, 2.0), 2.0, Arc::clone(&lightc));
    v.push(Box::new(light1));
    v.push(Box::new(light2));
    v.push(Box::new(light3));
    v.push(Sphere::boxed(V3::new(0.0, -1000.0, 0.0), 1000.0, checker));
    v.push(Box::new(Teapot::new(
        V3::new(0.0, ground_level + scale / 2.0, 4.0),
        scale,
        Quaternion::identity(),
        &Material::lambertian(ConstantTexture::boxed(V3::new(0.1, 0.2, 0.5))),
    )));
    v.push(Box::new(Teapot::new(
        V3::new(0.0, ground_level + scale / 2.0, -4.0),
        scale,
        Quaternion::identity(),
        &Material::metal(ConstantTexture::boxed(V3::new(0.7, 0.6, 0.5)), 0.0),
    )));
    v.push(Box::new(Teapot::new(
        V3::new(0.0, ground_level + scale / 2.0, 0.0),
        scale,
        Quaternion::identity(),
        &Material::dielectric(1.5),
    )));

    let origin = V3::new(14.0, 1.5, -22.0);
    let at = V3::new(0.0, 1.0, -0.5);
    let dist_to_focus = (origin - at).norm();
    let aperture = 0.001;
    let camera = Camera::new(
        origin,
        at,
        V3::new(0.0, 1.0, 0.0),
        15.0,
        aspect,
        aperture,
        dist_to_focus,
    );
    let density: Vec<Box<Hitable>> = vec![Box::new(light1c), Box::new(light2c), Box::new(light3c)];
    (Scene::new(Box::new(Tree::create(v, 3)), density), camera)
}
