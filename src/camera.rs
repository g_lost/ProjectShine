use ray::V3;
use ray::Ray;
use std::f64::consts::PI;
use rand;

#[derive(Debug)]
pub struct Camera {
    origin: V3,
    lower_left_corner: V3,
    horizontal: V3,
    vertical: V3,
    left: V3,
    backwards : V3,
    up: V3,
    lens_radius: f64
}


impl Camera {
    pub fn new(origin: V3, look_at: V3, vup: V3, vfov: f64, aspect:f64, aperture: f64, focus_dist: f64) -> Camera {
        let lens_radius = aperture / 2.0;
        let theta = vfov * PI/180.0;
        let half_height = (theta/2.0).tan();
        let half_width = aspect * half_height;
        let backwards = (origin - look_at).normalize();
        let left = vup.cross(&backwards).normalize();
        let up = backwards.cross(&left);
        let lower_left_corner = origin - half_width*focus_dist*left - half_height*focus_dist*up - focus_dist*backwards;
        let horizontal = 2.0 * half_width * focus_dist * left;
        let vertical = 2.0 * half_height * focus_dist *up;

        Camera {
            origin,
            lower_left_corner,
            horizontal,
            vertical,
            left,
            backwards,
            up,
            lens_radius
        }
    }

    pub fn get_ray(&self, u: f64, v:f64) -> Ray {
        let rd = self.lens_radius * Camera::random_in_unit_disk();
        let offset = self.left * rd.x + self.up * rd.y;
        Ray::new(self.origin + offset, self.lower_left_corner + u*self.horizontal + v*self.vertical - self.origin - offset)
    }

    fn random_in_unit_disk() -> V3 {
        loop {
            let p = 2.0 * V3::new(rand::random::<f64>(), rand::random::<f64>(), 0.0) - V3::new(1.0, 1.0, 0.0);
            if p.dot(&p) >= 1.0 { continue; } else { break p; }
        }
    }
}