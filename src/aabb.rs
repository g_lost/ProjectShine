//NOTE(goost) Source  https://github.com/andystanton/raytracer-rs/blob/master/src/aabb.rs
use ray::Ray;

use ray::V3;

use std::mem::swap;

#[derive(Copy, Clone)]
pub struct AABB {
    pub min: V3,
    pub max: V3,
}

impl AABB {
    pub fn new(min: V3, max: V3) -> Self {
        Self {
            min,
            max,
        }
    }

    pub fn surface_area(&self) -> f64{
        let size = self.max - self.min;
        return 2.0 * (size.x*size.y + size.x*size.z + size.y*size.z);
    }
    pub fn surrounding_box(&self, other: &AABB) -> AABB {
        Self::new(
            V3::new(self.min.x.min(other.min.x), self.min.y.min(other.min.y), self.min.z.min(other.min.z)),
            V3::new(self.max.x.max(other.max.x), self.max.y.max(other.max.y), self.max.z.max(other.max.z)),
        )
    }

    pub fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> bool {
        for a in 0..3 {
            let inv_d = 1.0 / r.direction()[a];
            let mut t0 = (self.min[a] - r.origin()[a]) * inv_d;
            let mut t1 = (self.max[a] - r.origin()[a]) * inv_d;

            if inv_d < 0.0 {
                swap(&mut t0, &mut t1)
            }

            let t_min_2 = if t0 > t_min {
                t0
            } else {
                t_min
            };

            let t_max_2 = if t1 < t_max {
                t1
            } else {
                t_max
            };

            if t_max_2 <= t_min_2 {
                return false;
            }
        }
        true
    }
}