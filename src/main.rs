extern crate project_shine;

use std::env;

fn main() -> Result<(), std::io::Error> {
    let args = env::args().skip(1).collect::<Vec<String>>();
    project_shine::run(&args)
    }
