use aabb::AABB;
use hitable::HitRecord;
use hitable::Hitable;
use ray::Ray;
use material::Material;
use light::Light;
use ray::V3;

pub struct Scene {
    hitables: Box<Hitable>,
    density: Vec<Box<Hitable>>,
}

impl Scene {
    pub fn new(hitables: Box<Hitable>, density: Vec<Box<Hitable>>) -> Scene {
        Scene { hitables, density }
    }
}

impl Scene {
    pub fn get_density(&self) -> &Vec<Box<Hitable>> {
        &self.density
    }
}

impl Hitable for Scene {
     fn pdf_value(&self, o: &V3, v: &V3) -> f64 {
        self.hitables.pdf_value(o, v)
    }

    fn random(&self, o: &V3) -> V3 {
        self.hitables.random(o)
    }

    fn count_primitives(&self) -> u32 {
        self.hitables.count_primitives()
    }

    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        self.hitables.hit(r, t_min, t_max)
    }

    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        self.hitables.bounding_box(t0, t1)
    }
}
