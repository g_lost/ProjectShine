use na::Vector3;
use ray::V3;

pub trait Texture: Send + Sync {
    fn value(&self, u: f64, v: f64, p: &V3) -> V3;
}

pub struct ConstantTexture {
    color: V3,
}

#[allow(dead_code)]
impl ConstantTexture {
    pub fn new(color: V3) -> ConstantTexture {
        ConstantTexture { color }
    }

    pub fn boxed(color: V3) -> Box<ConstantTexture> {
        Box::new(ConstantTexture { color })
    }

    pub fn from_rgb(c: &Vector3<u8>) -> ConstantTexture {
        let color = V3::new(
            f64::from(c.x) / 255.0,
            f64::from(c.y) / 255.0,
            f64::from(c.z) / 255.0,
        );
        ConstantTexture { color }
    }
}

impl Texture for ConstantTexture {
    fn value(&self, _u: f64, _v: f64, _p: &V3) -> V3 {
        self.color
    }
}

pub struct CheckerTexture {
    odd: Box<Texture>,
    even: Box<Texture>,
}

#[allow(dead_code)]
impl CheckerTexture {
    pub fn new(odd: Box<Texture>, even: Box<Texture>) -> CheckerTexture {
        CheckerTexture { odd, even }
    }

    pub fn boxed(odd: Box<Texture>, even: Box<Texture>) -> Box<CheckerTexture> {
        Box::new(CheckerTexture { odd, even })
    }
}

impl Texture for CheckerTexture {
    fn value(&self, u: f64, v: f64, p: &V3) -> V3 {
        let sines = (10.0 * p.x).sin() * (10.0 * p.y).sin() * (10.0 * p.z).sin();
        if sines < 0.0 {
            self.odd.value(u, v, p)
        } else {
            self.even.value(u, v, p)
        }
    }
}
