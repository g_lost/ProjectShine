use ray::V3;
use onb::ONB;
use f64::consts::PI;
use rand;
use hitable::Hitable as ha;


pub trait Pdf: Send + Sync {
    fn value(&self,direction: &V3) -> f64;
    fn generate(&self) ->V3;
}

pub struct Cosine {
    uvw: ONB
}

pub fn random_to_sphere(radius: f64, distance_squared: f64) -> V3 {
    let r1 = rand::random::<f64>();
    let r2 = rand::random::<f64>();
    let z = 1.0 + r2 * ((1.0-radius*radius/distance_squared).sqrt()-1.0);
    let phi = 2.0 * PI * r1;
    let x = phi.cos() * (1.0-z*z).sqrt();
    let y = phi.sin() * (1.0-z*z).sqrt();
    V3::new(x,y,z)
}

impl Cosine {
    pub fn new(w: &V3) -> Self {
        Cosine {
            uvw: ONB::build_from_w(w)
        }
    }

    fn random_cosine_direction() -> V3 {
        let r1 = rand::random::<f64>();
        let r2 = rand::random::<f64>();
        let z = (1.0-r2).sqrt();
        let phi = 2.0 * PI * r1;
        let x = phi.cos() * 2.0 * r2.sqrt();
        let y = phi.sin() * 2.0 * r2.sqrt();
        V3::new(x,y,z)
    }
}

impl Pdf for Cosine {
    fn value(&self, direction: &V3) -> f64 {
        let cosine = direction.normalize().dot(&self.uvw.w());
        if cosine > 0.0 {
            cosine / PI
        } else {
            0.0
        }
    }
    fn generate(&self) ->V3 {
        self.uvw.local_vec(&Cosine::random_cosine_direction())
    }
}


pub struct Hitable <'a> {
    origin: V3,
    hitable: &'a ha
}

impl <'a> Hitable<'a> {
    pub fn new (hitable: &'a ha, origin: V3) -> Self {
        Hitable {hitable, origin}
    }
}

impl <'a> Pdf for Hitable<'a> {
    fn value(&self,direction: &V3) -> f64 {
        self.hitable.pdf_value(&self.origin, direction)
    }
    fn generate(&self) ->V3 {
        self.hitable.random(&self.origin)
    }
}

pub struct Mixture<'a> {
    pdf1: &'a Pdf,
    pdf2: &'a Pdf
}

impl <'a> Mixture<'a> {
    pub fn new(pdf1: &'a Pdf, pdf2: &'a Pdf) -> Self {
        Mixture {pdf1, pdf2}
    }
}

impl <'a> Pdf for Mixture<'a> {
    fn value(&self,direction: &V3) -> f64 {
        0.5 * self.pdf1.value(direction) + 0.5 * self.pdf2.value(direction)
    }
    fn generate(&self) ->V3 {
        if rand::random::<f64>() < 0.5 {
            self.pdf1.generate()
        } else {
            self.pdf2.generate()
        }
    }
}