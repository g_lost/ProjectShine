use ray::V3;
use std::ops::Index;
use std::f64;

pub struct ONB {
    u: V3,
    v: V3,
    w: V3
}

impl Index<u32> for ONB {
    type Output = V3;

    fn index(&self, i: u32) -> &V3 {
        match i {
            0 => &self.u,
            1 => &self.v,
            2 => &self.w,
            _ => panic!("Only u, v ,w!")
        }
    }
}

impl ONB {
    pub fn u(&self) -> V3 {
        self.u
    }

    pub fn v(&self) -> V3 {
        self.v
    }

    pub fn w(&self) -> V3 {
        self.w
    }

    pub fn local(&self, a: f64, b: f64, c: f64) -> V3 {
        a*self.u + b * self.v + c * self.w
    }

    pub fn local_vec(&self, a: &V3) -> V3 {
        a.x*self.u + a.y * self.v + a.z *self.w
    }

    pub fn build_from_w(n: &V3) -> Self{
        let w = n.normalize();
        let a =             if f64::abs(w.x) > 0.9 {
                V3::new(0.0,1.0,0.0)
            } else {
                V3::new(1.0,0.0,0.0)
            };
        let v = w.cross(&a).normalize();
        let u = w.cross(&v);
        ONB {u, v, w}
    }


}