use aabb::AABB;
use hitable::{HitRecord, Hitable};
use rand;
use ray::Ray;

use std::cmp::Ordering;
use std::collections::HashMap;

pub enum Tree {
    EmptyLeaf,
    Branch {
        left: Box<Tree>,
        right: Box<Tree>,
        bounding_box: AABB,
    },
    Leaf {
        entry: Vec<Box<Hitable>>,
    },
}

impl Hitable for Tree {
    fn count_primitives(&self) -> u32 {
        match self {
            &Tree::EmptyLeaf => 0,
            Tree::Leaf { entry } => entry.count_primitives(),
            Tree::Branch {
                left,
                right,
                bounding_box: _,
            } => left.count_primitives() + right.count_primitives(),
        }
    }

    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        match self {
            &Tree::EmptyLeaf => None,
            Tree::Leaf { entry } => entry.hit(r, t_min, t_max),
            Tree::Branch {
                left,
                right,
                bounding_box,
            } => match bounding_box.hit(r, t_min, t_max) {
                true => {
                    let hit_left = left.hit(r, t_min, t_max);
                    let hit_right = right.hit(r, t_min, t_max);
                    match (hit_left, hit_right) {
                        (Some(lefty), Some(righty)) => {
                            if lefty.t < righty.t {
                                Some(lefty)
                            } else {
                                Some(righty)
                            }
                        }
                        (Some(lefty), None) => Some(lefty),
                        (None, Some(righty)) => Some(righty),
                        _ => None,
                    }
                }
                false => None,
            },
        }
    }

    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        match self {
            &Tree::EmptyLeaf => None,
            Tree::Leaf { entry } => entry.bounding_box(t0, t1),
            Tree::Branch {
                left: _,
                right: _,
                bounding_box,
            } => Some(*bounding_box),
        }
    }
}

fn box_axis_compare(axis: usize) -> Box<Fn(&Box<Hitable>, &Box<Hitable>) -> Ordering> {
    Box::new(move |a: &Box<Hitable>, b: &Box<Hitable>| {
        a.bounding_box(0.0, 0.0)
            .and_then(|box_left| {
                b.bounding_box(0.0, 0.0).map(|box_right| {
                    if box_left.min[axis] - box_right.min[axis] < 0.0 {
                        Ordering::Less
                    } else {
                        Ordering::Greater
                    }
                })
            })
            .unwrap_or_else(|| {
                panic!("no bounding box in bvh node constructor");
            })
    })
}

impl Tree {
    pub fn create(mut l: Vec<Box<Hitable>>, max_elements: usize) -> Self {
        match l.len() {
            u if u <= max_elements => Tree::Leaf { entry: l },
            /* 2 => {
                let r = l.pop().unwrap();
                let l = l.pop().unwrap();
                let bb = match (r.bounding_box(0.0, 0.0), l.bounding_box(0.0,0.0)) {
                    (Some(bb1), Some(bb2)) => bb1.surrounding_box(&bb2),
                    //NOTE (goost) Other cases should never happen?
                    (Some(bb1), None) => bb1,
                    (None, Some(bb2)) => bb2,
                    _ => panic!("Should never happen")
                };
                Bvh::Branch {left: Bvh::Leaf{entry: l}, right: Bvh::Leaf{entry: r}, bounding_box: bb}
            }*/
            0 => Tree::EmptyLeaf,
            _ => {
                let axis = (3.0 * rand::random::<f64>()) as usize;
                l.sort_by(&*box_axis_compare(axis));

                let mut costs: Vec<(usize,f64)> = (0..l.len()).map(|i| (i, Tree::calculate_cost(&l, i))).collect();
                let lcost = costs[l.len()/2];    
                costs.sort_by(|a, b| a.1 .partial_cmp(&b.1).unwrap());
                let split_pos = costs[0].0;
                //let split_pos = l.len() / 2;
               // println!("SplitPos {}, Cost: {}, Len/2 {}, Cost {:?}", split_pos, costs[0].1, l.len() /2, lcost);
                let mut v = Vec::with_capacity(l.len() - split_pos);
                for _i in split_pos..l.len() {
                    v.push(l.pop().unwrap())
                }
                
                let right = Tree::create(l, max_elements);
                let left = Tree::create(v, max_elements);
                let bb = right
                    .bounding_box(0.0, 0.0)
                    .unwrap()
                    .surrounding_box(&left.bounding_box(0.0, 0.0).unwrap());
                Tree::Branch {
                    right: Box::new(right),
                    left: Box::new(left),
                    bounding_box: bb,
                }
            }
        }
    }

    fn calculate_cost(l: &[Box<Hitable>], split_pos: usize) -> f64 {
        /*Cost(cell) = C_trav + Prob(hit L) * Cost(L) + Prob(hit R) * Cost(R)
                    = C_trav + SA(L) * TriCount(L) + SA(R) * TriCount(R)*/

        let left = &l[..split_pos];
        let right = &l[split_pos..];
        let right_area = right.bounding_box(0.0,0.0).map(|b| b.surface_area()).unwrap_or(0.0);
        let left_area = left.bounding_box(0.0,0.0).map(|b| b.surface_area()).unwrap_or(0.0);
        left_area * f64::from(left.count_primitives())  + right_area * f64::from(right.count_primitives())
    }
}
