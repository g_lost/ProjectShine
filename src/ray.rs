use na::Vector3;
use na::UnitQuaternion as NAQuaternion;

pub type V3 = Vector3<f64>;
pub type Quaternion =  NAQuaternion<f64>;

#[derive(Debug, Copy, Clone)]
pub struct Ray {
    origin: V3,
    direction: V3,
}

impl Ray {
    pub fn new(origin: V3, direction: V3) -> Ray {
        Ray { origin, direction }
    }

    pub fn origin(&self) -> V3 {
        self.origin
    }
    pub fn direction(&self) -> V3 {
        self.direction
    }
    pub fn point_at(&self, x: f64) -> V3 {
        self.origin + x * self.direction
    }
}
