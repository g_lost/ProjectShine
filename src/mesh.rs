use aabb::AABB;
use hitable::HitRecord;
use hitable::Hitable;
use material::Material;
use ray::Quaternion;
use ray::Ray;
use ray::V3;
use std::path::Path;
use std::sync::Arc;
use tobj;
use tree::Tree;
use triangle::Triangle;

pub struct Mesh {
    //hitables: Vec<Box<Hitable>>
    hitables: Box<Hitable>,
}

#[allow(dead_code)]
impl Mesh {
    pub fn new(
        path: &str,
        center: V3,
        scale: f64,
        rotation: Quaternion,
        material: &Arc<Material>,
    ) -> Self {
        Mesh {
            hitables: load_obj(path, center, scale, rotation, material),
        }
    }
}

impl Hitable for Mesh {
    fn count_primitives(&self) -> u32 {
        self.hitables.count_primitives()
    }

    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        self.hitables.hit(r, t_min, t_max)
    }

    fn bounding_box(&self, t0: f64, t1: f64) -> Option<AABB> {
        self.hitables.bounding_box(t0, t1)
    }
}

fn load_obj(
    path: &str,
    center: V3,
    scale: f64,
    rotation: Quaternion,
    material: &Arc<Material>,
) -> Box<Hitable> {
    let raw_model = tobj::load_obj(&Path::new(path));
    assert!(raw_model.is_ok());
    let (models, materials) = raw_model.unwrap();

    println!("# of models: {}", models.len());
    println!("# of materials: {}", materials.len());

    let v: Vec<Box<Hitable>> = models
        .iter()
        .enumerate()
        .map(|(i, m)| {
            println!("Reading Mesh: {}:{}", i, m.name);
            let mesh = &m.mesh;
            assert!(mesh.positions.len() % 3 == 0);
            assert!(mesh.indices.len() % 3 == 0);

            let hitables: Vec<Box<Hitable>> =
            //let v: Vec<Box<Hitable>> =
            (0..mesh.indices.len() /3).map(|i| {
              //  println!("Getting indices.");
                
                let i1 = mesh.indices[3 * i] as usize;
                let i2 = mesh.indices[3 * i + 1] as usize;
                let i3 = mesh.indices[3 * i + 2] as usize;
              //  println!("    idx[{}] = {}, {}, {}.", i, i1,i2,i3);

            //    println!("Creating vecs.");
                let v1 = V3::new(
                    mesh.positions[i1*3] as f64,
                    mesh.positions[i1*3+1] as f64,
                    mesh.positions[i1*3+2] as f64);
                //    println!("    v[{}] = ({}, {}, {})", i1,v1.x,v1.y, v1.z);

                let v2 = V3::new(
                    mesh.positions[i2*3] as f64,
                    mesh.positions[i2*3+1] as f64,
                    mesh.positions[i2*3+2] as f64);
            //    println!("    v[{}] = ({}, {}, {})", i2, v2.x,v2.y, v2.z);
                let v3 = V3::new(
                    mesh.positions[i3*3] as f64,
                    mesh.positions[i3*3+1] as f64,
                    mesh.positions[i3*3+2] as f64);
             //   println!("    v[{}] = ({}, {}, {})", i3, v3.x,v3.y, v3.z);
             //   println!("Creating normals.");
                let n1 = V3::new (
                    mesh.normals[i1*3] as f64,
                    mesh.normals[i1*3+1] as f64,
                    mesh.normals[i1*3+2] as f64);
             //   println!("    n[{}] = ({}, {}, {})", i1, n1.x,n1.y, n1.z);
                let n2 = V3::new (
                    mesh.normals[i2*3] as f64,
                    mesh.normals[i2*3+1] as f64,
                    mesh.normals[i2*3+2] as f64);
              //  println!("    n[{}] = ({}, {}, {})", i2, n2.x,n2.y, n2.z);
                let n3 = V3::new (
                    mesh.normals[i3*3] as f64,
                    mesh.normals[i3*3+1] as f64,
                    mesh.normals[i3*3+2] as f64);
             //   println!("    n[{}] = ({}, {}, {})", i3, n3.x,n3.y, n3.z);
            //    println!("Mapping to triangles.");
            //    println!("Triangle(v1({:?}), v2({:?}), v3({:?}, n1({:?}),n2({:?}),n3({:?}))",
              //      rotation*(v1 * scale)+center,rotation*(v2 * scale)+center,rotation*(v3 * scale)+center,
              //      rotation * n1,rotation * n2,rotation * n3 );
                Triangle::boxed(
                    [rotation*(v1 * scale)+center,
                    rotation*(v2 * scale)+center,
                    rotation*(v3 * scale)+center],
                    [rotation * n1,rotation * n2,rotation * n3],
                    Arc::clone(&material)
                ) as Box<Hitable>
            }).collect();
            hitables
        })
        .flatten()
        .collect();
    Box::new(Tree::create(v, 15))
}
