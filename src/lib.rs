#![feature(iterator_flatten)]
extern crate image;
extern crate indicatif;
extern crate nalgebra as na;
extern crate ordered_float;
extern crate piston_window;
extern crate rand;
extern crate rayon;
extern crate tobj;

use image::{Rgba, RgbaImage};
use piston_window::*;
use piston_window::Texture as PistonTexture;

use indicatif::{ProgressBar, ProgressStyle};
use rayon::prelude::*;
use std::f64;
use std::sync::Arc;
use std::time::Instant;

mod camera;
mod hitable;
mod material;
mod ray;
mod scene;
mod sphere;
mod texture;
mod triangle;
mod teapot;
mod aabb;
mod tree;
mod light;
mod mesh;
mod scenes;
mod pdf;
mod onb;

use camera::Camera;
use hitable::Hitable;
use material::Material;
use material::ScatterRecord;
use ray::Ray;
use scene::Scene;
use sphere::Sphere;
use ray::V3;
use texture::CheckerTexture;
use texture::ConstantTexture;
use teapot::Teapot;
use ray::Quaternion;
use tree::Tree;
use light::Light;
use pdf::Pdf;
use mesh::Mesh;

type Color = Rgba<u8>;

pub fn run(args: &[String]) -> Result<(), std::io::Error> {


    let (nx, ny, ns, filepath, scene) = match args {
        [first, second, third, fourth, fifth] => (
            first.as_str(),
            second.as_str(),
            third.as_str(),
            fourth.as_str(),
            fifth.as_str(),
        ),
        [first, second, third, fourth] => (
            first.as_str(),
            second.as_str(),
            third.as_str(),
            fourth.as_str(),
            "default",
        ),
        _ => ("800", "600", "1", "default.png", "default"),
    };
    let nx = u32::from_str_radix(&nx[..], 10).unwrap();
    let ny = u32::from_str_radix(&ny[..], 10).unwrap();
    let ns = u32::from_str_radix(&ns[..], 10).unwrap();

    println!("Tracing...");
    let aspect = f64::from(nx) / f64::from(ny);
    let (scene, camera) = match scene {
        "random" => scenes::random(aspect),
        "teapot" => scenes::teapot(aspect),
        "simpleI" => scenes::simple_import(aspect),
        "dungeon" => scenes::dungeon(aspect),
        _ => scenes::default(aspect),
    };
    let start_time = Instant::now();
    let pixel_data = generate_color_vector(nx, ny, ns, &scene, &camera);
    println!("Creating image...");
    let img = generate_image(&pixel_data);
    let total = start_time.elapsed();

    println!(
        "Took {}s {}ms.\nWriting png...",
        total.as_secs(),
        total.subsec_millis()
    );
    write_png_image(filepath, &img)?;
    println!("Success! Showing window.");
    let mut window: PistonWindow = WindowSettings::new("Tracer", [nx, ny])
        .exit_on_esc(true)
        .build()
        .unwrap();
    window.set_lazy(true);

    let trace_image: G2dTexture =
        PistonTexture::from_image(&mut window.factory, &img, &TextureSettings::new()).unwrap();
    //let empty_image: G2dTexture = Texture::empty(&mut window.factory).unwrap();
    while let Some(e) = window.next() {
        window.draw_2d(&e, |c, g| {
            clear([0.0; 4], g);
            image(&trace_image, c.transform, g);
        });
    }

    Ok(())
}
fn generate_image(colors: &[Vec<Color>]) -> RgbaImage {
    let ny = colors.len() as u32;
    let nx = colors[0].len() as u32;
    let mut img = image::RgbaImage::new(nx, ny);
    for (y, column) in colors.iter().enumerate() {
        for (x, pixel) in column.iter().enumerate() {
            img.put_pixel(x as u32, y as u32, *pixel)
        }
    }
    img
}

fn write_png_image(filepath: &str, img: &image::RgbaImage) -> Result<(), std::io::Error> {
    img.save(filepath)?;
    Ok(())
}

fn generate_color_vector(nx: u32, ny: u32, ns: u32, scene: &Scene, camera: &Camera) -> Vec<Vec<Color>> {
    //NOTE(goost) Currently used as infinite tick
    let progress_bar = ProgressBar::new(u64::from(nx) * u64::from(ny) * u64::from(ns));
    progress_bar.set_style(ProgressStyle::default_bar().template("[{elapsed} elapsed]"));
    //Tick in BG thread and not hinder tracing
    progress_bar.enable_steady_tick(1000);
    let result: Vec<Vec<Color>> = (0..ny)
        .rev()
        .map(|y| {
            (0..nx)
                .into_par_iter()
                .map(|x| {
                    (0..ns).into_par_iter()
                        .map(|_s| {
                            let u = (f64::from(x) + rand::random::<f64>()) / f64::from(nx);
                            let v = (f64::from(y) + rand::random::<f64>()) / f64::from(ny);
                            let ray = camera.get_ray(u, v);
                            let c = vec_color(&ray, scene,0);
                            remove_nan(&c)
                        })
                        /*
                        .fold((1.0,V3::zeros()),
                              |(i,avg), c| (i+1.0, (avg * (i-1.0) + c ) / i)).1
                        */
                        .fold(|| (1.0,V3::zeros()),
                              |(i,avg), c| (i+1.0, (avg * (i-1.0) + c ) / i))
                        .reduce(|| (1.0,V3::zeros()),
                              |(i,avg), (_, c)| (i+1.0, (avg * (i-1.0) + c ) / i)).1
                })
                .map(vec3_to_color)
                .collect::<Vec<Color>>()
        })
        .collect();
    progress_bar.finish();
    result
}

fn remove_nan(mut v: &V3) -> V3 {
    let x = if v.x.is_nan() {0.0} else {v.x};
    let y = if v.y.is_nan() {0.0} else {v.y};
    let z = if v.z.is_nan() {0.0} else {v.z};
    V3::new(x,y,z)
}

fn vec3_to_color(color_vec: V3) -> Color {
    let cap = |x: f64| if x > 1.0 {1.0} else {x};
    //let color_vec = V3::new(cap(color_vec.x.sqrt()), cap(color_vec.y.sqrt()), cap(color_vec.z.sqrt()));
    let color_vec = V3::new(color_vec.x.sqrt(), color_vec.y.sqrt(), color_vec.z.sqrt());

    //let color_vec = V3::new(cap(color_vec.x), cap(color_vec.y), cap(color_vec.z));
    let color_vec = if color_vec.x > 1.0 || color_vec.y > 1.0 || color_vec.z > 1.0 {
        let max = color_vec.x.max(color_vec.y).max(color_vec.z);
        V3::new(color_vec.x/max,color_vec.y/max,color_vec.z/max)
    } else {
        color_vec
    };
    let r = (255.99 * color_vec.x) as u8;
    let g = (255.99 * color_vec.y) as u8;
    let b = (255.99 * color_vec.z) as u8;
    Rgba([r, g, b, 255])
}

fn vec_color(r: &Ray, scene: &Scene, depth: u8) -> V3 {
    match scene.hit(r, 0.001, f64::MAX) {
        Some(ref rec) /*if depth < 50*/ => match (rec.material.scatter)(r, rec) {
            Some(ScatterRecord(specular_ray, is_specular, attenuation, pdf_ref)) => {
                if depth < 50 {
                    if(is_specular){
                        //NOTE(goost) This implementation appears to produces broken results
                    //shadow_ray(&attenuation, &rec.pos, &rec.normal, scene) + (rec.material.emitted)(rec.u, rec.v, &rec.pos) +
                    vec_color(&specular_ray, scene, depth + 1).component_mul(&attenuation)
                    } else{
                        let hpdf = pdf::Hitable::new(scene.get_density(), rec.pos);
                        let b = &*pdf_ref.unwrap();
                        let mpdf = pdf::Mixture::new(&hpdf,b);
                        let scattered = ray::Ray::new(rec.pos, mpdf.generate());
                        let emitted = (rec.material.emitted)(r, rec,rec.u, rec.v, &rec.pos);
                        let pdf_val = mpdf.value(&scattered.direction());
                        //vec_color(&specular_ray, scene, depth + 1).component_mul(&attenuation)
                        emitted + vec_color(&specular_ray, scene, depth + 1).component_mul(&(attenuation*(rec.material.scatter_pdf)(r,rec,&scattered)))/pdf_val
                   }
                } else {
                //    shadow_ray(&attenuation, &rec.pos, &rec.normal, scene) +
                    (rec.material.emitted)(r, rec,rec.u, rec.v, &rec.pos)
                }
            }
            None => (rec.material.emitted)(r, rec, rec.u, rec.v, &rec.pos),
        },
        Some(ref rec) =>
           // shadow_ray(&rec.material, &rec.pos, &rec.normal, scene) +
                (rec.material.emitted)(r, rec, rec.u, rec.v, &rec.pos),
        //NOTE (goost) If blackground is something other then black
        /*None => {
           let unit_direction = r.direction().normalize();
            let t = 0.5 * (unit_direction.y + 1.0);
            (1.0 - t) * V3::new(1.0, 1.0, 1.0) + t * V3::new(0.5, 0.7, 1.0)
        }*/
        None => V3::new(0.000,0.00,0.000)
    }
}
