use ray::Ray;
use hitable::HitRecord;
use ray::V3;
use rand;
use std::sync::Arc;
use texture::Texture;
use pdf::Pdf;
use std::f64::consts;
use pdf;

pub struct ScatterRecord(pub Ray,pub bool, pub V3,pub Option<Box<Pdf>>);

pub struct Material {
    pub scatter: Box<(Fn(&Ray,&HitRecord) -> Option<ScatterRecord>) + Sync + Send>,
    pub scatter_pdf: Box<(Fn(&Ray,&HitRecord,&Ray) -> f64) + Sync + Send>,
    pub emitted: Box<(Fn(&Ray,&HitRecord,f64,f64, &V3) -> V3) + Sync + Send >
}


impl Material {
    pub fn lambertian(albedo: Box<Texture>) -> Arc<Material> {
        Arc::new(
            Material {
                scatter: Box::new(move |_r_in: &Ray, rec: &HitRecord| {
                    let target = rec.pos + rec.normal + Material::random_in_unit_sphere();
                      //TODO (goost) UV Coords
                      let cos_pdf = Box::new(pdf::Cosine::new(&rec.normal)) as Box<Pdf>;
                    Some(ScatterRecord(Ray::new(rec.pos, target - rec.pos),false, albedo.value(0.0, 0.0, &rec.pos), Some(cos_pdf)))
                }),
                scatter_pdf: Box::new(move |r_in: &Ray, rec: &HitRecord, scattered: &Ray| {
                    let cosine = rec.normal.dot(&scattered.direction().normalize());
                    if cosine < 0.0 {
                        0.0
                    } else {
                        cosine / consts::PI
                    }
                }),
                emitted: Box::new(move |_,_,_,_,_| {
                    V3::zeros()
                })
            }
        )
    }

    pub fn metal (albedo: Box<Texture>, fuzz: f64) -> Arc<Material> {
        let fuzz = if fuzz < 1.0 {fuzz} else {1.0};
        Arc::new(
            Material {
                scatter: Box::new(move |r_in: &Ray, rec: &HitRecord| {
                    let reflected = Material::reflect(&r_in.direction().normalize(), &rec.normal);
                    let scattered = Ray::new(rec.pos, reflected + fuzz * Material::random_in_unit_sphere());
                    if scattered.direction().dot(&rec.normal) > 0.0 {
                        //TODO (goost) UV Coords
                        Some(ScatterRecord(scattered, true, albedo.value(0.0, 0.0, &rec.pos), None))
                    } else {
                        None
                    }
                }),
                scatter_pdf: Box::new(move |_,_,_| { 0.0}),
                emitted: Box::new(move |_,_,_,_,_| {
                    V3::zeros()
                })
            }
        )
    }

    pub fn diffuse_light(emit: Box<Texture>, intensity: f64) -> Arc<Material> {
        Arc::new (
            Material {
                scatter: Box::new(move |_, _| {
                    None
                }),
                scatter_pdf: Box::new(move |_,_,_| { 0.0}),
                emitted: Box::new(move |r_in: &Ray, rec: &HitRecord, u:f64,v:f64,p: &V3| {
                    if rec.normal.dot(&r_in.direction()) < 0.0 {
                        emit.value(u, v, p) * intensity
                    }
                    else {
                        V3::zeros()
                    }
                })
            }
        )
    }

    pub fn dielectric (ref_idx: f64) -> Arc<Material> {
        Arc::new (
            Material {
                scatter: Box::new (move |r_in: &Ray, rec: &HitRecord| {
                    let attenuation = V3::new(1.0, 1.0, 1.0);
                    //let distance = (r_in.direction() * rec.t).norm();
                    //let attenuation = attenuation;
                    //let attenuation = V3::new(attenuation.x.exp(), attenuation.y.exp(), attenuation.z.exp());
                    let reflected = Material::reflect(&r_in.direction(), &rec.normal);
                    let (outward_normal, ni_over_nt, cosine) =
                        if r_in.direction().dot(&rec.normal) > 0.0 {
                            let cosine = ref_idx * r_in.direction().dot(&rec.normal) / r_in.direction().norm();
                            //let cosine = r_in.direction().dot(&rec.normal) / r_in.direction().norm();
                            //let cosine = (1.0 - ref_idx*ref_idx*(1.0 - cosine*cosine)).sqrt();
                            (-rec.normal, ref_idx, cosine)
                        }
                         else {
                            let cosine = -r_in.direction().dot(&rec.normal) / r_in.direction().norm();
                            (rec.normal, 1.0/ref_idx, cosine)
                        };
                    let (out_ray, reflect_prob) = match Material::refract(&r_in.direction(), &outward_normal, ni_over_nt) {
                        Some(refracted) =>{
                            (Ray::new(rec.pos, refracted), Material::schlick(cosine, ref_idx))
                            },
                        None => {
                            (Ray::new(rec.pos, reflected), 1.0)
                        }
                    };
                    if rand::random::<f64>() < reflect_prob {
                        Some(ScatterRecord(Ray::new(rec.pos, reflected),true, attenuation, None))
                    } else {
                        Some(ScatterRecord(out_ray, true,attenuation, None))
                    }
                }),
                scatter_pdf: Box::new(move |_,_,_| { 0.0}),
                emitted: Box::new(move |_,_,_,_,_| {
                    V3::zeros()
                })
            }
        )
    }

    fn schlick(cosine: f64, ref_idx: f64) -> f64 {
        let r0 = (1.0 - ref_idx) / (1.0 + ref_idx);
        let r0 = r0 * r0;
        r0 + (1.0 - r0)*(1.0 - cosine).powi(5)
    }

    fn refract(v: &V3, n: &V3, ni_over_nt: f64) -> Option<V3> {
        let uv = v.normalize();
        let dt = uv.dot(n);
        let discriminant =  1.0 - ni_over_nt*ni_over_nt*(1.0 - dt*dt);
        if discriminant > 0.0 {
            Some(ni_over_nt * (uv - n*dt) - n * discriminant.sqrt())
        }
        else {
            None
        }
    }

    fn reflect(v: &V3, n: &V3) -> V3 {
        v - 2.0* v.dot(n) * n
    }

    fn random_in_unit_sphere() -> V3 {
        loop {
            let p = 2.0 * V3::new(rand::random::<f64>(), rand::random::<f64>(), rand::random::<f64>()) - V3::new(1.0, 1.0, 1.0);
            if p.norm_squared() >= 1.0 {
                continue;
            }
                else {
                    break p;
                }
        }
    }
}